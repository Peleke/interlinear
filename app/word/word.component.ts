import {Component, Input} from '@angular/core';
import {ITranslation} from '../common/ITranslation';

@Component({
  selector : 'word',
  template : require('raw-loader!./word.component.html'),
  styles   : [require('raw-loader!./word.component.css')]
})
export class WordComponent {
  @Input() word : ITranslation;
}
