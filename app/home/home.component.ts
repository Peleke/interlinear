import {Component, ViewChild} from '@angular/core';

import {InputComponent} from '../input/input.component';
import {TranslationsComponent} from '../translations/translations.component';

import {TranslationService} from '../common/translation.service';

@Component({
  selector    : 'home',
  template    : require('raw-loader!./home.component.html'),
  styles      : [require('raw-loader!./home.component.css')],
  directives  : [InputComponent, TranslationsComponent],
  providers   : [TranslationService]
})
export class HomeComponent {

  @ViewChild(TranslationsComponent)
  private _translationsComponent : TranslationsComponent;

  public _words : string[] = [];
  
  constructor (private _translationService : TranslationService) { }

  // Initiates vocabuary list refresh in TranslationsComponent.
  public refreshTranslations() {
    // Bridge
    this._translationsComponent.refreshTranslations(this._words);
    console.log('Bridged.');
  }

  // @params words   Deduped raw text of user's textual input.
  public cacheWords(words : string[]) : void {
    this._words = words; 
  }

}
