import 'core-js';
import 'zone.js/dist/zone';

import {bootstrap} from '@angular/platform-browser-dynamic';
import {ROUTER_PROVIDERS} from '@angular/router';
import {HTTP_PROVIDERS, JSONP_PROVIDERS} from '@angular/http';

import {RootComponent} from './root.component';

bootstrap(RootComponent, 
          [ROUTER_PROVIDERS, HTTP_PROVIDERS, JSONP_PROVIDERS]);
