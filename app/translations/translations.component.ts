import {Component} from '@angular/core';
import {WordComponent} from '../word/word.component.ts';

import {Observable, Subject} from 'rxjs';

import {TranslationService} from '../common/translation.service';
import {ITranslation} from '../common/ITranslation';

@Component({
  selector : 'translations',
  template : require('raw-loader!./translations.component.html'),
  styles   : [require('raw-loader!./translations.component.css')],
  directives: [WordComponent],
  providers: [TranslationService]
})
export class TranslationsComponent {

  public words : Subject<ITranslation[]>;

  private _translations$ : Subject<Map<string, ITranslation[]>>;

  private _translations  : Map<string, ITranslation[]>;

  constructor (private _translationService : TranslationService) {
    this.words = new Subject<ITranslation[]>();
    this._translations$ = new Subject<Map<string, ITranslation[]>>();
    this._translations  = new Map<string, ITranslation[]>();
  }

  public refreshTranslations (words : string[]) : void {
    this._translationService.fetchTranslations(words, this._translations, this.words);
  }
}
