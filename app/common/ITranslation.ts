export interface ITranslation {
  search_term       : string;
  ipa_pronunciation : string; 
  part_of_speech?   : string;
  translation       : string; 
}
