import {CONFIG} from '../config';
import {Jsonp} from '@angular/http';

import {Injectable} from '@angular/core';
import {ITranslation} from './ITranslation';

import {Observable, Subject} from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

const uniq = require('ramda').uniq;

type TranslationMap = Map<string, ITranslation[]>;

@Injectable()
export class TranslationService {

  private BASE_URL : string = `http://www.dictionaryapi.com`;

  private _words         : string[] = [];
  private _translations : Map<string, ITranslation[]> = new Map<string, ITranslation[]>();

  public translations$ : Subject<Map<string, ITranslation[]>>;

  constructor (private _jsonp : Jsonp) {
    this.translations$ = new Subject<Map<string, ITranslation[]>>();
  }

  fetchTranslations (_words : string[], map : TranslationMap, subject : Subject<ITranslation[]>) : void {
    map.clear();
    
      _words.forEach((word) => {
          this._jsonp
           .get(this.fetchYqlUrl(word))
           .map((json)         => this.extractEntries(json))
           .filter((extracted) => extracted != undefined)
           .map((extracted)    => this._put(word, this.parseApiResponse(extracted), map))
           .map((trans_map)    => Array.from(map.values()))
           .map((value_arr)    => {
             return value_arr.map((translation_arr) => translation_arr[0]) 
            })
           .subscribe((val)    => subject.next(val));
           //.subscribe(()       => console.log(`CTr: ${JSON.stringify(map)}`));
          })
  }

  refreshTranslations () : void {
    this._translations.clear()

    this._words.forEach((word) => {
        this._jsonp
         .get(this.fetchYqlUrl(word))
         .map((json)         => this.extractEntries(json))
         .filter((extracted) => extracted != undefined)
         .map((extracted)    => this.put(word, this.parseApiResponse(extracted)))
         .subscribe(() => console.log(`Tr: ${JSON.stringify(this._translations)}`));
        })
  }

  private extractEntries (json : any) : any {
    return json['_body']['query']['results']['body']['entry_list'];
  }

  // Returns URL to lookup a given word.
  fetchYqlUrl (word : string) : string {
      const root      = 'https://query.yahooapis.com/v1/public/yql?q=',
            term      = this.endpoint(word),
            yql       = `select * from html where url='${term}'`,
            params    = `&format=json&diagnostics=false`,
            proxy_url = `${root}${encodeURIComponent(yql)}${params}&callback=JSONP_CALLBACK`;

      return proxy_url;
  }

  private endpoint(search_term : string) : string {
    // TODO :: Sanitize w/ encodeURIComponent
    return `${this.BASE_URL}/api/v1/references/spanish/xml/${search_term}?key=${CONFIG.KEY}`;
  }

  updateSpanishVocabulary (raw_text : string) : void  {
    this._words =
      uniq(raw_text.toLowerCase().split(' '))
        .filter((word) => word.length !== 0)
        .map((word) => word = word.replace(/[!,.:;?]/, ""))
        .filter((word : string) => {
          return (word.search(/["#$%&'()*+-/<=>@\^_`{|}~]/) === -1)
        });
  }

  // @param  entry (Object[]) Badly named list of entries.
  // @return any[]    An array of ITranslation objects for the term that
  //                    generated entry argument
  private parseApiResponse ({ entry }) : ITranslation[] {
    return entry
      .map((entry) => {
         return {
           search_term       : entry['id'].replace(/\[\d*\]/, ""),
           ipa_pronunciation : this.simplifyProperty(entry['pr']),
           part_of_speech    : this.simplifyProperty(entry['fl']),
           translation       : this.extractDefinitions(entry)
        }
     })
  }

  private simplifyProperty (property : any) : string {
    if (Array.isArray(property)) {
      return property[0];
    } else {
      return property;
    }
  }

  // This is repulsive.
  private extractDefinitions (definition_data : any[]) : any {
    definition_data = definition_data['def'];

    // console.log(`Data: ${ JSON.stringify(definition_data) }`);
    if (!definition_data || Array.isArray(definition_data['dt'])) {
      return;
    } else if (Array.isArray(definition_data)) {
      const def = this.handleNested(definition_data);
      // console.log(def);
      return def;
    } else if (definition_data['dt']) {
      const def = definition_data['dt']['ref-link'];
      // console.log(`R: ${ def }`);
      return def;
    }
  }

  // @param definition_data   Node with potentially nested definition data.
  private handleNested (definition_data : any) : any {
    return definition_data
             .filter((data)  => !Array.isArray(data))
             .filter((data)  => data['dt'])
             .filter((data)  => !Array.isArray(data['dt']))
             .map((data)     => data['dt']['rel-link']);
  }

  // @param word      Spanish word associated with translations.
  // @param translations Array of translation data.
  private put(word : string, translations : ITranslation[]) : void {
    this._translations.set(word, translations);
    this.translations$.next(this._translations);
  }

  public _put(word : string, trans : ITranslation[], map : TranslationMap) : TranslationMap {
    map.set(word, trans);
    return map;
  }
}
