import {Component, Output, EventEmitter} from '@angular/core';
import {TranslationService} from '../common/translation.service';

const uniq = require('ramda').uniq;

@Component({
  selector : 'text-input',
  template : require('raw-loader!./input.component.html'),
  styles   : [require('raw-loader!./input.component.css')],
  providers: [TranslationService]
})
export class InputComponent {

  @Output() wordsEmitter : EventEmitter<string[]> = new EventEmitter<string[]>();

  private _words : string[] = [];

  constructor (private _translationService : TranslationService) { }

  // @param raw_text    Raw string value of user's textual input.
  cacheWords(raw_text : string) : void {
    this._words =
      uniq(raw_text.toLowerCase().split(' '))
        .filter((word) => word.length !== 0)
        .map((word) => word = word.replace(/[!,.:;?]/, ""))
        .filter((word : string) => {
          return (word.search(/["#$%&'()*+-/<=>@\^_`{|}~]/) === -1)
        });

    this.wordsEmitter.emit(this._words);
  }

  updateSpanishVocabulary (raw_text : string) : void  {
    this._translationService.updateSpanishVocabulary(raw_text);
  }

}
