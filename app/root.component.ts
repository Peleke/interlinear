import {Component} from '@angular/core';
import {Routes, ROUTER_DIRECTIVES} from '@angular/router';

import {HomeComponent} from './home/home.component';
import {TranslationsComponent} from './translations/translations.component';

@Component({
  selector   : 'root',
  template   : require('raw-loader!./root.component.html'),
  styles     : [require('raw-loader!./root.component.css')],
  directives : [ROUTER_DIRECTIVES, HomeComponent, TranslationsComponent]
})
@Routes([
  { path : '/',         component : HomeComponent},
  { path : '/home',     component : HomeComponent},
  { path : '/assisted', component : TranslationsComponent}
])
export class RootComponent { }
