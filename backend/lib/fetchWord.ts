'use strict';

const request       = require('request'),
      parseString   = require('xml2js').parseString;

/**
 * Application Constants
 */

const HOST = 'http://www.dictionaryapi.com',
      KEY  = '714cd7cb-8e4f-4561-a161-c9d7f2879c4f';

const getEndpoint = function (word : string) : string {
  return `${HOST}/api/v1/references/spanish/xml/${word}?key=${KEY}`
} 

const makeRequest = function (word : string) : void {
  request(getEndpoint(word), (err, res, body) => {
    parseString(body, (err, res) => console.dir(JSON.stringify(res)));
  })
}

console.log(makeRequest('fish'));

module.exports = makeRequest;
