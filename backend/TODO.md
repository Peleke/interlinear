# TODO

The app's main functionality is to collect translations for the words that appear in a Spanish-language text. This entails:

1. Calling the server for each word's dictionary entry;
2. Collecting the English definitions for each word into a sensible structure; and
3. Aggregating those objects into something easily manipulated in the view.

That's pretty general, as written, but the details will get clearer as we work through this.

Here are a few things for you to do:

1. Extract the word, IPA pronunciation, and English definitions from the server response JSON;
2. Create a "sensible object" with the data; and
3. Remove duplicate words from the list of all words in the text.

## 1. Parsing JSON

The first of these will probably take you a bit. Not because it's hard, but because MW's API response is nasty.

[This JSON formatter](https://jsonformatter.curiousconcept.com/) will make it easier to understand and work with.

## 2. Extracting translation information

A "sensible object" would probably look like this:

{
  "spanish_word"  : { spanish word },
  "pronunciation" : { IPA pronunciation }
  "translations"  : [{ english definitions }]
}

. . . But feel free to get creative.

## 3. Stripping unique values

You can go through the trouble of doing this yourself, but that would be stupid. Instead, use Ramda's uniq function, which literally does this for you.

I know it's silly to task you with "use this function from a library", but there's a reason. I want you to to do the following:

1. Install Ramda. How do you install an NPM package? How do you save it as a development dependency? 
2. Think of a good way to structure your modules. What file should this stripDuplicates function live in?
3. In that file, import Rambda. Look up Node require statements.
4. Write the function.
5. As a bonus, write this function in [TypeScript](https://www.typescriptlang.org/docs/tutorial.html).

# Testing

We're using [ Testem ](https://github.com/testem/testem) and [Jasmine](http://jasmine.github.io/) for testing.

To write tests:

1. Create a testfile in lib/spec according to the pattern [module name]-spec.js;
2. Open a new terminal,  navigate to interlinear/backend, and run: testem
3. Get to editing. Your tests will run whenever you update any JS in lib.
