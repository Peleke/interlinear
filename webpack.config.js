module.exports = {
  entry: './app/boot.ts',
  debug: true,
  resolve: {
    extensions : ['', '.ts', '.js']
  },

  output: {
    path: './build',
    filename: 'bundle.js'
  },

  module: {
    loaders: [
      { test : /\.ts$/, loader: 'awesome-typescript' }
    ]
  },

  devServer: {
    historyApiFallback: true
  }
}
